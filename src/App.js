import React from 'react';
import './App.css';
import Form_ from './components/Form_';
import {Route, withRouter, Switch} from 'react-router-dom';
import AllFlight from './components/AllFlight';
class App extends React.Component {

    render(){

  return (
    <div className="App">
     
          <Switch>
             <Route path = '/flight-search' component = {Form_}/>
             <Route path = '/' component = {AllFlight}/>
        </Switch>
             
        
    </div>
  );
}
}
export default App;
