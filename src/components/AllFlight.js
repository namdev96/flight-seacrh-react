import React from 'react';
import Button from 'react-bootstrap/Button'
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";

import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Form';
import Col from 'react-bootstrap/Form';
import Container from 'react-bootstrap/Container'
import { Table, Tag, Space,DatePicker, message, Spin,InputNumber,Collapse,Image  } from 'antd';
import dateFormat from "dateformat";
import download from './download.jpg';
import { Menu } from 'antd';
const { Panel } = Collapse;

function onChange(value) {
  console.log('changed', value);
}
class AllFlight extends React.Component {
state = {
  data:[],
  date:'',
  loader: true,
  value:'',
  value_:'',
  flight:[],
  maxPrice:null,
  minPrice:null
}
	 componentDidMount(){
    fetch("https://tw-frontenders.firebaseio.com/advFlightSearch.json")
      .then(response => response.json())
      .then(responseData =>{
        console.log(responseData)
        this.setState(
          {
            data: responseData,
            loader:false
            
          }
          
        )
      }
      );
  }
 selectedValue_origin = e =>{
 	const select_value = e.target.value
 	this.setState({
 		value:select_value
 	})
 }
 selectedValue_destination = e =>{
 	const select_value = e.target.value
 	this.setState({
 		value_:select_value
 	})
 }
 onDateChange = (e) =>{

    this.setState({
      date: dateFormat(e, "yyyy/mm/dd")
    });
 }
handleSubmit = (event) =>{
 event.preventDefault()
	const value = this.state.value
	const value_ = this.state.value_
	const date = this.state.date

	console.log("valiue of origin and destination is",value, value_)
	const search = this.state.data.filter(obj => obj.origin == value && obj.destination == value_&&obj.date == date);
    console.log("Verify*****************",search);
    this.setState({
    	loader:false
    })
    this.setState({
      flight: search
      
    });
   
}
minPrice = e =>{
 
	this.setState({
		minPrice :e.target.value
	})
	}
	maxPrice = e =>{
    
	this.setState({
		maxPrice :e.target.value
	})
	}

	priceFilter = e =>{
		e.preventDefault()
		const min = this.state.minPrice;
		const max = this.state.maxPrice;
		const data = this.state.data;
		const filtered_price = data.filter(obj =>
			obj.price>=min || obj.price<=max
		)
		console.log("Filtered Price is: ", filtered_price)
	}
	render(){
		const flight = this.state.flight;

const columns = [
      {
        title: <b>Arrival Time</b>,
        key: "arrivalTime",
        dataIndex: "arrivalTime",
      
      },

      {
        title: <b>Departure Time</b>,
        dataIndex: "departureTime",
        key: "departureTime",
       
       sorter: (a, b) => {
          const departA = a.departureTime,
            departB = b.departureTime;
          console.log("value of dateA", departA);
          console.log("value of dateB", departB);
          return departA - departB;
        },
        ellipsis: true,
    },
      {
        title: <b>Flight No</b>,
        dataIndex: "flightNo",
        key: "flightNo",
        render: name => {
           return <Tag color="red">{name}</Tag>;
                 }
       
      },
      {
        title: <b>Name</b>,
        dataIndex: "name",
        key: "name",
        render: name => {
           return <Tag color="dodgerblue">{name}</Tag>;
                 }

      },
      {
        title: <b>Price</b>,
        key: "price",
        dataIndex: "price",
        

         sorter: (a, b) => {
          const priceA = a.price,
           priceB = b.price;
          console.log("value of dateA", priceA);
          console.log("value of dateB", priceB);
          return priceA - priceB;
        },
         render:  design =>{
         return <Tag color="green">{design}</Tag>;}
      }
      
    ];
//
	//	// let destination_option = []
  //   if (this.state.data.length > 0) {
  //     this.state.data.forEach(city => {
  //       let cities = {}
  //       city.value = city.id
  //       cities.label = city.destination
  //       destination_option.push(cities)
  //     })
  //   }
  //   let origin_option = []
  //   if (this.state.data.length > 0) {
  //     this.state.data.forEach(city => {
  //       let cities = {}
  //       city.value = city.id
  //       cities.label = city.origin
  //       origin_option.push(cities)
  //     })
  //   }
		return(
 <Container>
 <Navbar bg="light" expand="lg">
   <Navbar.Brand href="#home">
      <img
        src={download}
       width='25%'
        className="d-inline-block align-top"
        alt="err"
      />
    </Navbar.Brand>
  <Navbar.Toggle aria-controls="basic-navbar-nav" />
  <Navbar.Collapse id="basic-navbar-nav">
    <Nav className="mr-auto">
     <Nav.Link href="#flight">
      <span className="nav-text" >Flights</span>
      <Link to="/flight"/>
      </Nav.Link>
      <Nav.Link href="#flight-search">
      <span className="nav-text" style = {{backgroundColor:'DodgerBlue', padding:10,color:'white',fontWeight:'bold',borderRadius:'5px',marginLeft:'10px',hover:'blue'}}>Search Flight</span>
      <Link to="/flight-search"/>
      
     </Nav.Link>
</Nav>
  
  </Navbar.Collapse>
</Navbar>
 
{ this.state.loader ? (
          <Spin style={{ marginLeft: "50%", marginTop: "15%" }} />
        ) : (
    
               <Table
                  rowKey={data => data.id}
                  columns={columns}
                  dataSource={this.state.data}
                  className="table"
                  style={{ marginTop: 10 }}
                  size="small"
                  pagination={{
                    showSizeChanger: true,
                    onShowSizeChange: this.onShowSizeChange,
                    defaultCurrent: 1,
                    defaultPageSize: 10,
                    showQuickJumper: true,
                    showTotal: (total, range) =>
                      `${range[0]}-${range[1]} of ${total} items`,
                    pageSizeOptions: ["10", "20", "30", "40", "50"]
                  }}
                  scroll={{ x: true }}
                  
                />
                
                )}
               
</Container>
)
}
}
export default AllFlight;
